import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';

part 'locations.g.dart';

@JsonSerializable()
class LatLng {
  LatLng({
    this.lat,
    this.lng,
  });

  factory LatLng.fromJson(Map<String, dynamic> json) => _$LatLngFromJson(json);
  Map<String, dynamic> toJson() => _$LatLngToJson(this);

  final double lat;
  final double lng;
}

@JsonSerializable()
class Parking {
  Parking({
    this.place,
    this.text,
    this.lat,
    this.lng,
  });

  factory Parking.fromJson(Map<String, dynamic> json) => _$ParkingFromJson(json);
  Map<String, dynamic> toJson() => _$ParkingToJson(this);

  final String place;
  final String text;
  final double lat;
  final double lng;
}

@JsonSerializable()
class Nature_trail {
  Nature_trail({
    this.place,
    this.web,
    this.lat,
    this.lng,
    this.travel,
    this.text,
  });

  factory Nature_trail.fromJson(Map<String, dynamic> json) => _$Nature_trailFromJson(json);
  Map<String, dynamic> toJson() => _$Nature_trailToJson(this);

  final String place;
  final String web;
  final double lat;
  final double lng;
  final String travel;
  final String text;
}

@JsonSerializable()
class Campfire {
  Campfire({
    this.place,
    this.text,
    this.lat,
    this.lng,
  });

  factory Campfire.fromJson(Map<String, dynamic> json) => _$CampfireFromJson(json);
  Map<String, dynamic> toJson() => _$CampfireToJson(this);

  final String place;
  final String text;
  final double lat;
  final double lng;
}

@JsonSerializable()
class Museum_monument {
  Museum_monument({
    this.place,
    this.web,
    this.text,
    this.lat,
    this.lng,
  });

  factory Museum_monument.fromJson(Map<String, dynamic> json) => _$Museum_monumentFromJson(json);
  Map<String, dynamic> toJson() => _$Museum_monumentToJson(this);

  final String place;
  final String web;
  final String text;
  final double lat;
  final double lng;
}

@JsonSerializable()
class Locations {
  Locations({
    this.parking,
    this.nature_trail,
    this.campfire,
    this.museum_monument,
  });

  factory Locations.fromJson(Map<String, dynamic> json) =>
      _$LocationsFromJson(json);
  Map<String, dynamic> toJson() => _$LocationsToJson(this);

  final List<Parking> parking;
  final List<Nature_trail> nature_trail;
  final List<Campfire> campfire;
  final List<Museum_monument> museum_monument;
}

Future<Locations> getGoogleNature_trail() async {
  const googleLocationsURL = 'https://jonipeltomaa.gitlab.io/kotisivut/nature_flutter.json';

  // Retrieve the locations of Google offices
  final response = await http.get(googleLocationsURL);
  if (response.statusCode == 200) {
    return Locations.fromJson(json.decode(response.body));
  } else {
    throw HttpException(
        'Unexpected status code ${response.statusCode}:'
            ' ${response.reasonPhrase}',
        uri: Uri.parse(googleLocationsURL));
  }
}