// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'locations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LatLng _$LatLngFromJson(Map<String, dynamic> json) {
  return LatLng(
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$LatLngToJson(LatLng instance) => <String, dynamic>{
      'lat': instance.lat,
      'lng': instance.lng,
    };

Parking _$ParkingFromJson(Map<String, dynamic> json) {
  return Parking(
    place: json['place'] as String,
    text: json['text'] as String,
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$ParkingToJson(Parking instance) => <String, dynamic>{
      'place': instance.place,
      'text': instance.text,
      'lat': instance.lat,
      'lng': instance.lng,
    };

Nature_trail _$Nature_trailFromJson(Map<String, dynamic> json) {
  return Nature_trail(
    place: json['place'] as String,
    web: json['web'] as String,
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
    travel: json['travel'] as String,
    text: json['text'] as String,
  );
}

Map<String, dynamic> _$Nature_trailToJson(Nature_trail instance) =>
    <String, dynamic>{
      'place': instance.place,
      'web': instance.web,
      'lat': instance.lat,
      'lng': instance.lng,
      'travel': instance.travel,
      'text': instance.text,
    };

Campfire _$CampfireFromJson(Map<String, dynamic> json) {
  return Campfire(
    place: json['place'] as String,
    text: json['text'] as String,
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CampfireToJson(Campfire instance) => <String, dynamic>{
      'place': instance.place,
      'text': instance.text,
      'lat': instance.lat,
      'lng': instance.lng,
    };

Museum_monument _$Museum_monumentFromJson(Map<String, dynamic> json) {
  return Museum_monument(
    place: json['place'] as String,
    web: json['web'] as String,
    text: json['text'] as String,
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$Museum_monumentToJson(Museum_monument instance) =>
    <String, dynamic>{
      'place': instance.place,
      'web': instance.web,
      'text': instance.text,
      'lat': instance.lat,
      'lng': instance.lng,
    };

Locations _$LocationsFromJson(Map<String, dynamic> json) {
  return Locations(
    parking: (json['parking'] as List)
        ?.map((e) =>
            e == null ? null : Parking.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    nature_trail: (json['nature_trail'] as List)
        ?.map((e) =>
            e == null ? null : Nature_trail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    campfire: (json['campfire'] as List)
        ?.map((e) =>
            e == null ? null : Campfire.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    museum_monument: (json['museum_monument'] as List)
        ?.map((e) => e == null
            ? null
            : Museum_monument.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$LocationsToJson(Locations instance) => <String, dynamic>{
      'parking': instance.parking,
      'nature_trail': instance.nature_trail,
      'campfire': instance.campfire,
      'museum_monument': instance.museum_monument,
    };
